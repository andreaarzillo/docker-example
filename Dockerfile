FROM java:8-jdk
RUN mkdir /myapp
COPY src/Docker.java /myapp
WORKDIR /myapp
RUN javac Docker.java
CMD java Docker